package main

import (
	"fmt"
	"log"
)

func main() {
	// parse settings
	if err := settingsInstance.Load(); err != nil {
		log.Fatalln("unable to load settings file", err)
	}

	// check mode
	if settingsInstance.Mode != modeCombined && settingsInstance.Mode != modeProxy && settingsInstance.Mode != modeServer {
		log.Fatalln("invalid mode " + settingsInstance.Mode)
	}

	// check for secret
	if settingsInstance.Mode == modeServer && settingsInstance.ProxySecret == "" {
		log.Fatalln("missing setting proxySecret")
	}

	// connect to receiver
	if settingsInstance.Mode == modeCombined || settingsInstance.Mode == modeServer {
		receiverHost := fmt.Sprintf("%s://%s:%d", settingsInstance.ReceiverWebifProtocol, settingsInstance.ReceiverHost, settingsInstance.ReceiverWebifPort)
		if !receiverInstance.Init(receiverHost) {
			log.Fatalln("error during receiver initialization")
		}
	}

	// start webserver
	var webServer WebServerInterface
	if settingsInstance.Mode == modeProxy {
		webServer = NewWebServerProxy()
	} else {
		webServer = NewWebServer()
	}
	if err := webServer.Run(settingsInstance.WebServerListen); err != nil {
		log.Fatalln("unable to start webserver", err)
	}
}
