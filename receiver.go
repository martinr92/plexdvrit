package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

type Receiver struct {
	ServerAddress string
	Tuners        int
	Services      receiverServices
}

var receiverInstance = &Receiver{}

func (receiver *Receiver) Init(serverAddress string) bool {
	receiver.ServerAddress = serverAddress

	if !receiver.loadReceiverInfo() {
		return false
	}
	if !receiver.loadAllServices() {
		return false
	}

	return true
}

func (receiver *Receiver) loadData(path string, object interface{}) bool {
	// build URL
	newURL, err := url.Parse(receiver.ServerAddress)
	if err != nil {
		log.Println("unable to build receiver URL", receiver.ServerAddress, err)
		return false
	}
	newURL.Port()
	newURL.Path = path

	// call receiver
	response, err := http.Get(newURL.String())
	if err != nil {
		log.Println("unable to call receiver", err)
		return false
	}
	defer response.Body.Close()

	// receive response
	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("unable to receive http response body", err)
		return false
	}

	// unmarshall response
	if err := json.Unmarshal(data, object); err != nil {
		log.Println("unable to unmarshal JSON response", err, string(data))
		return false
	}

	// everything OK
	return true
}

func (receiver *Receiver) loadReceiverInfo() bool {
	var receiverData receiverAbout
	if !receiver.loadData("api/about", &receiverData) {
		log.Println("error during receiver info loading")
		return false
	}

	// store amount of tuners
	receiver.Tuners = len(receiverData.Info.Tuners)
	log.Println(receiver.Tuners, "tuner detected")
	return true
}

func (receiver *Receiver) loadAllServices() bool {
	var receiverData receiverServices
	if !receiver.loadData("api/getallservices", &receiverData) {
		log.Println("error during services loading")
		return false
	}

	// store services
	receiver.Services = receiverData
	log.Println(receiver.Services.ServiceAmount(), "services found")
	return true
}

type receiverAbout struct {
	Info receiverAboutInfo
}

type receiverAboutInfo struct {
	Tuners []receiverAboutInfoTuner
}

type receiverAboutInfoTuner struct {
	Name string
}

type receiverServices struct {
	Services []receiverService
}

func (services receiverServices) ServiceAmount() int {
	response := 0
	for _, service := range services.Services {
		response += len(service.SubServices)
	}
	return response
}

func (services receiverServices) ByID(id int) (service receiverSubService, found bool) {
	for _, currentService := range receiverInstance.Services.Services {
		for _, subService := range currentService.SubServices {
			if subService.Program != id {
				continue
			}
			return subService, true
		}
	}

	return
}

type receiverService struct {
	SubServices []receiverSubService
}

type receiverSubService struct {
	ServiceName      string
	Program          int
	ServiceReference string
}

func (service receiverSubService) InternalURL(request *http.Request) string {
	newURL := url.URL{
		Scheme: "http",
		Host:   request.Host,
		Path:   fmt.Sprintf("/service/%d", service.Program),
	}
	return newURL.String()
}

func (service receiverSubService) StreamingURL() string {
	newURL := url.URL{
		Scheme: "http",
		Host:   fmt.Sprintf("%s:%d", settingsInstance.ReceiverHost, settingsInstance.ReceiverStreamingPort),
		Path:   service.ServiceReference,
	}
	return newURL.String()
}
