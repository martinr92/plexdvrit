package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"

	httprouter "gitlab.com/martinr92/gohttprouter"
)

const (
	httpHeaderProxySecret     = "X-PlexDVRIt-Secret"
	httpHeaderContentType     = "Content-Type"
	httpHeaderContentTypeJson = "application/json"
)

type WebServerInterface interface {
	Run(addr string) error
}

type WebServer struct {
}

type WebServerProxy struct {
}

func NewWebServer() *WebServer {
	return &WebServer{}
}

func (webServer *WebServer) Run(addr string) error {
	router := httprouter.New()
	router.HandleFunc(http.MethodGet, "/discover.json", webServer.checkConnection(webServer.discoverJson))
	router.HandleFunc(http.MethodGet, "/lineup.json", webServer.checkConnection(webServer.lineupJson))
	router.HandleFunc(http.MethodGet, "/lineup_status.json", webServer.checkConnection(webServer.lineupStatusJson))
	router.HandleFunc(http.MethodGet, "/service/:id", webServer.checkConnection(webServer.service))

	log.Println("starting webserver on", addr)
	return http.ListenAndServe(addr, router)
}

func (webServer *WebServer) checkConnection(fnc httprouter.RouteHandler) httprouter.RouteHandler {
	return func(rw http.ResponseWriter, r *http.Request, ri httprouter.RoutingInfo) {
		// check for authentication
		if settingsInstance.Mode == modeServer {
			secret := r.Header.Get(httpHeaderProxySecret)
			if secret != settingsInstance.ProxySecret {
				rw.WriteHeader(http.StatusForbidden)
				return
			}
		}

		log.Println("request", r.URL.Path)
		fnc(rw, r, ri)
	}
}

func (webServer *WebServer) discoverJson(response http.ResponseWriter, request *http.Request, routingInfo httprouter.RoutingInfo) {
	discover := Discover{
		FriendlyName:    "PlexDVRIt",
		Manufacturer:    "Martin Riedl",
		ManufacturerUrl: "https://www.martin-riedl.de",
		FirmwareName:    version,
		TunerCount:      receiverInstance.Tuners,
	}
	data, err := json.Marshal(discover)
	if err != nil {
		log.Println(err)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}
	response.Header().Add(httpHeaderContentType, httpHeaderContentTypeJson)
	response.Write(data)
}

func (webServer *WebServer) lineupJson(response http.ResponseWriter, request *http.Request, routingInfo httprouter.RoutingInfo) {
	lineup := []LineUp{}
	for _, service := range receiverInstance.Services.Services {
		for _, subService := range service.SubServices {
			lineup = append(lineup, LineUp{
				GuideName:   subService.ServiceName,
				GuideNumber: fmt.Sprintf("%d", subService.Program),
				URL:         subService.InternalURL(request),
			})
		}
	}

	data, err := json.Marshal(lineup)
	if err != nil {
		log.Println(err)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}
	response.Header().Add(httpHeaderContentType, httpHeaderContentTypeJson)
	response.Write(data)
}

func (webServer *WebServer) lineupStatusJson(response http.ResponseWriter, request *http.Request, routingInfo httprouter.RoutingInfo) {
	response.Header().Add(httpHeaderContentType, httpHeaderContentTypeJson)
	response.Write([]byte("{}"))
}

func (webServer *WebServer) service(response http.ResponseWriter, request *http.Request, routingInfo httprouter.RoutingInfo) {
	// find service
	idString := routingInfo.Parameters["id"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		return
	}
	service, found := receiverInstance.Services.ByID(id)
	if !found {
		response.WriteHeader(http.StatusNotFound)
		return
	}

	// build remote URL
	streamingURL := service.StreamingURL()
	log.Println("start streaming from", streamingURL)

	// connect to receiver
	streamResponse, err := http.Get(streamingURL)
	if err != nil {
		log.Println(err)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer streamResponse.Body.Close()

	// prepare buffering
	byteChannel := make(chan []byte, 128)
	senderClosedChannel := make(chan bool)
	go webServer.sendBuffer(byteChannel, senderClosedChannel, response)
	defer close(byteChannel)

	// process body
	bytes := make([]byte, 1024*10)
	for {
		// read bytes
		cntBytes, err := streamResponse.Body.Read(bytes)
		if err != nil {
			log.Println(err)
			return
		}

		// copy bytes
		bytesCopy := make([]byte, cntBytes)
		copy(bytesCopy, bytes)

		// send bytes
		if len(byteChannel) == cap(byteChannel) {
			log.Println("internal buffer is full; check connectivity!")
		}
		byteChannel <- bytesCopy

		select {
		case <-senderClosedChannel:
			return
		default:
		}
	}
}

func (webSercer *WebServer) sendBuffer(byteChannel chan []byte, senderClosedChannel chan bool, response http.ResponseWriter) {
	for {
		// wait for next bytes
		bytes, more := <-byteChannel
		if !more {
			break
		}

		// send bytes
		_, err := response.Write(bytes)
		if err != nil {
			log.Println(err)
			break
		}
	}

	// notify parent
	senderClosedChannel <- true
}

func NewWebServerProxy() *WebServerProxy {
	return &WebServerProxy{}
}

func (webServer *WebServerProxy) Run(addr string) error {
	router := httprouter.New()
	router.HandleFunc(http.MethodGet, "/discover.json", webServer.proxyFile)
	router.HandleFunc(http.MethodGet, "/lineup.json", webServer.proxyLineup)
	router.HandleFunc(http.MethodGet, "/lineup_status.json", webServer.proxyFile)
	router.HandleFunc(http.MethodGet, "/service/:id", webServer.proxyFile)

	// start webserver
	log.Println("starting webserver on", addr)
	return http.ListenAndServe(addr, router)
}

func (webServer *WebServerProxy) proxyLineup(response http.ResponseWriter, request *http.Request, routingInfo httprouter.RoutingInfo) {
	// build  proxy URL
	newRequest := webServer.buildProxyURL(response, request)
	if newRequest == nil {
		return
	}

	// send new request to server
	newResponse, err := http.DefaultClient.Do(newRequest)
	if err != nil {
		log.Println(err)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer newResponse.Body.Close()

	// check http status
	if newResponse.StatusCode != http.StatusOK {
		log.Println("invalid http status", newResponse.StatusCode)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}

	// read json data
	originalJsonData, err := io.ReadAll(newResponse.Body)
	if err != nil {
		log.Println(err)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}

	// parse json
	var lineup []LineUp
	if err := json.Unmarshal(originalJsonData, &lineup); err != nil {
		log.Println(err)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}

	// modify lineup URL
	responseLineup := []LineUp{}
	for _, entry := range lineup {
		// replace URL
		originalURL, err := url.Parse(entry.URL)
		if err != nil {
			log.Println(err)
			response.WriteHeader(http.StatusInternalServerError)
			return
		}
		originalURL.Host = request.Host
		entry.URL = originalURL.String()

		responseLineup = append(responseLineup, entry)
	}

	// marshal response
	responseData, err := json.Marshal(responseLineup)
	if err != nil {
		log.Println(err)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}

	response.Header().Add(httpHeaderContentType, httpHeaderContentTypeJson)
	response.Write(responseData)
}

func (webServer *WebServerProxy) buildProxyURL(response http.ResponseWriter, request *http.Request) *http.Request {
	// build new URL
	newURL, err := url.Parse(settingsInstance.ProxyAddress)
	if err != nil {
		log.Print(err)
		response.WriteHeader(http.StatusInternalServerError)
		return nil
	}
	newURL.Path = request.URL.Path
	log.Println("proxy to", newURL.String())

	// set additional request data
	newRequest, err := http.NewRequest(request.Method, newURL.String(), nil)
	if err != nil {
		log.Println(err)
		response.WriteHeader(http.StatusInternalServerError)
		return nil
	}
	newRequest.Header.Add(httpHeaderProxySecret, settingsInstance.ProxySecret)
	return newRequest
}

func (webServer *WebServerProxy) proxyFile(response http.ResponseWriter, request *http.Request, routingInfo httprouter.RoutingInfo) {
	// build  proxy URL
	newRequest := webServer.buildProxyURL(response, request)
	if newRequest == nil {
		return
	}

	// send new request to server
	newResponse, err := http.DefaultClient.Do(newRequest)
	if err != nil {
		log.Println(err)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer newResponse.Body.Close()

	// check http status
	if newResponse.StatusCode != http.StatusOK {
		log.Println("invalid http status", newResponse.StatusCode)
		response.WriteHeader(http.StatusInternalServerError)
		return
	}

	// process response
	bytes := make([]byte, 1024*10)
	for {
		cntBytes, readError := newResponse.Body.Read(bytes)
		if _, writeError := response.Write(bytes[:cntBytes]); writeError != nil {
			log.Println(writeError)
			return
		}
		if readError != nil {
			log.Println(readError)
			return
		}
	}
}
