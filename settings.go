package main

import (
	"encoding/json"
	"io/ioutil"
)

const (
	modeCombined = "combined"
	modeProxy    = "proxy"
	modeServer   = "server"
)

type Settings struct {
	Mode                  string `json:"mode"`
	ReceiverHost          string `json:"receiverHost"`
	ReceiverWebifProtocol string `json:"receiverWebifProtocol"`
	ReceiverWebifPort     int    `json:"receiverWebifPort"`
	ReceiverStreamingPort int    `json:"receiverStreamingPort"`
	WebServerListen       string `json:"webServerListen"`
	ProxyAddress          string `json:"proxyAddress"`
	ProxySecret           string `json:"proxySecret"`
}

var settingsInstance = &Settings{
	Mode:                  modeCombined,
	ReceiverHost:          "localhost",
	ReceiverWebifProtocol: "http",
	ReceiverWebifPort:     80,
	ReceiverStreamingPort: 8001,
	WebServerListen:       ":34500",
}

func (settings *Settings) Load() error {
	// load settings
	data, err := ioutil.ReadFile("settings.json")
	if err != nil {
		return err
	}

	// parse settings
	if err := json.Unmarshal(data, settings); err != nil {
		return err
	}
	return nil
}
