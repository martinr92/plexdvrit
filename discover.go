package main

type Discover struct {
	FriendlyName    string
	Manufacturer    string
	ManufacturerUrl string
	FirmwareName    string
	TunerCount      int
}
