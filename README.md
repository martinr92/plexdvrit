[![pipeline status](https://gitlab.com/martinr92/plexdvrit/badges/main/pipeline.svg)](https://gitlab.com/martinr92/plexdvrit/-/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/martinr92/plexdvrit)](https://goreportcard.com/report/gitlab.com/martinr92/plexdvrit)

# PlexDVRIt
This application connects your Enigma2 based receiver with the Plex Media Server for Live TV & DVR.

# Setup
Download the latest release from [https://gitlab.com/martinr92/plexdvrit/-/releases](https://gitlab.com/martinr92/plexdvrit/-/releases).

## Modes
### Combined
Use the `combined` mode if the Plex Server and the receiver are on the same network. This is also the default mode.
```
+-----------------------------+                 +-----------------------------+
|   Receiver                  |                 |  Plex Server                |
|   192.168.1.101             |                 |  10.0.0.123                 |
|     :80    --> WebIf        |        LAN      |    + Receiver               |
|     :8001  --> Streaming    |   <---------->  |      + 192.168.1.101:34500  |
|                             |                 |                             |
|   PlexDVRIt                 |                 |                             |
|     :34500 --> Web          |                 |                             |
+-----------------------------+                 +-----------------------------+
```
1) Simply install this application on your receiver.
2) Add the Receiver (using the IP Adress) to the Plex Server.

### Server/Proxy
Otherwise you can use the `split` installation. This is recommended, if the Plex Server can not directly access to the receiver (typically if they are on different locations / networks). One part is running on your receiver (running in `server` mode). The other part is running on your Plex Server (in `proxy` mode).
```
+-----------------------------+                 +-----------------------------+
|   Receiver                  |                 |  Plex Server                |
|   192.168.1.101             |                 |  192.168.1.102              |
|     :80    --> WebIf        |        WAN      |    + Receiver               |
|     :8001  --> Streaming    |   <---------->  |      + 127.0.0.1:34500      |
|                             |   DynDNS        |                             |
|   PlexDVRIt                 |     myDynDNS    |  PlexDVRIt (Proxy)          |
|     :34500 --> Web          |                 |    ProxyAddress:            |
|                             |                 |      http://myDynDNS:34500  |
|                             |                 |    :34500 --> Web           |
+-----------------------------+                 +-----------------------------+
```
1) Install this application on your receiver (with mode `server`).
The following settins are required:
    - `mode` = `server`
    - `proxySecret` = `someSecretHashOrPassword`
2) Install this application on the Plex Server (with mode `proxy`).
The following settins are required:
    - `mode` = `proxy`
    - `proxyAddress` = `http://myDynDNS:34500`
    - `proxySecret` = `someSecretHashOrPassword` (same as set in step 1)
3) Make sure that the Proxy installation is able to connect to the Server installation (open port in firewall / router)

## Settings
Create a `settings.json` file for additional configuration. The following settings can be provided:
| Name                  | Default Value | Supported Modes      | Description
| --------------------- |-------------- | -------------------- | -----------
| mode                  | `combined`    |                      | Mode of the application<br />Choose between `combined`, `server` or `proxy`
| receiverHost          | `localhost`   | `combined`, `server` | Hostname or IP of the receiver
| receiverWebifProtocol | `http`        | `combined`, `server` | Prodocol used for connecting to receiver web interface
| receiverWebifPort     | `80`          | `combined`, `server` | Port used for connecting to redeiver web interfacee
| receiverStreamingPort | `8001`        | `combined`, `server` | Port used for connecting to receiver streaming interface
| proxyAddress          |               | `proxy`              | Address of the `server` when running in `proxy` mode
| proxySecret           |               | `server`, `proxy`    | Secret token for authentication between `server` and `proxy`<br />Use only the characters a-z, A-Z, 0-9 

# Contribution
Create merge requests always regarding the `develop` branch.
